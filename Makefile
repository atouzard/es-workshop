up:
	docker-compose up -d

down:
	docker-compose down

purge:
	@curl -s -XDELETE localhost:9200/products -o /dev/null
	@echo 'Index "products" purged'

populate:
	@curl -s -XPOST localhost:9200/products/_bulk --data-binary  @bulkInsert.db -H 'Content-Type: application/json' -o /dev/null
	@echo 'Index "products" populated'
