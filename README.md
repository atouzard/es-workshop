# Simple ES Docker project

## Setup

Up ⬆️
```
make up
```

Down ⬇️
```
make down
```

### Both services are available at those URLs :
- [Elasticsearch](http://localhost:9200)
- [Kibana](http://localhost:5601/app/dev_tools#/console)

## Kibana requests :

#### 1. Create Index
```elas
PUT /products
```
`produts` is the index name

#### 2. Create Index mapping
```
PUT /products/_mapping
{
  "properties": {
    "country":  {
      "type": "keyword"
    },
    "type": {
      "type": "keyword"
    },
    "comment": {
      "type": "text"
    }
  }
}
```

## CLI Commands :

#### 3. Populate index
```
make populate
```

#### 4. Purge index
```
make purge
```
